Built from the pytorch/pytorch:latest image, this Docker image adds the [lungmask](https://github.com/JoHof/lungmask) utility as well as a pre-loaded R231CovidWeb model.

To run lungmask from the command line:
```
docker run -v $PWD:/workspace xnat/lungmask:latest lungmask /workspace/sample.nii.gz /workspace/output_mask.nii.gz --modelname R231CovidWeb
```
